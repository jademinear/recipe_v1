﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;

using RecipesEDM;

namespace RecipeViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        RecipesContext context = new RecipesContext();
        public MainWindow() {

            
            InitializeComponent();
            
            using (RecipesContext context = new RecipesContext()) {
                List<Recipe> recipes = (from c in context.Recipes
                                        select c).ToList();

                ExportRecipes.ExportRecipesToXml(context);
            }

            
        }
    }
}
